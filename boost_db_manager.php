<?php


class boost_db_manager
{

    public function __construct($registry) {
		
       $this->db = $registry->get('db');

    }
   
    public $limit;
    public $select;
    public $sql;
    public $insert;
    public $update;
    public $order;
  
    public $selectOption;
    public $where;
  
    public $distinct;
   
    public $from;
   
    public $groupBy;
   
    public $join;
    /**
     * @var string|array the condition to be applied in the GROUP BY clause.
     * It can be either a string or an array. Please refer to [[where()]] on how to specify the condition.
     */
    public $having;

    public $union;
 
    public $params = [];
 
    public function prepare($builder)
    {
        return $this;
    }
   
    public function batch($batchSize = 100, $db = null)
    {
        return Yii::createObject([
            'class' => BatchQueryResult::className(),
            'query' => $this,
            'batchSize' => $batchSize,
            'db' => $db,
            'each' => false,
        ]);
    }
   
    public function each($batchSize = 100, $db = null)
    {
        return Yii::createObject([
            'class' => BatchQueryResult::className(),
            'query' => $this,
            'batchSize' => $batchSize,
            'db' => $db,
            'each' => true,
        ]);
    }
  
    public function  createCommand(){

        $this->sql ="";

        if(!empty($this->insert)){
            $this->sql.=$this->insert;
        }
        if(!empty($this->update)){
            $this->sql.=$this->update;
        }

        if(!empty($this->select) and is_array($this->select)){
            $this->sql.= "SELECT DISTINCT ". implode(',',$this->select);
        } else if(is_string($this->select)){
            $this->sql.= "SELECT DISTINCT * ";
        }
        if(!empty($this->from)){
            $this->sql.=" FROM ".implode(',',$this->from);
        }

        if(!empty($this->join)){
            foreach ($this->join as $arrJoin) {
                $this->sql.=$arrJoin[0].' '.$arrJoin[1]. ' on '. $arrJoin[2] ;
            }
        }

        if(!empty($this->where)){
            $i=0;
            $this->sql.=" WHERE ";
            foreach ($this->where as $key=>$value){
                if($i >0){
                    $this->sql.=" AND ";
                }
                $this->sql.=$key."=".$value;

                $i++;
            }


        }


        if(!empty($this->groupBy)){
            $this->sql.=" GROUP BY ".$this->groupBy;
        }
        if(!empty($this->order)){
            $this->sql.=$this->order;
        }

        if(!empty($this->limit)){
            $this->sql.=$this->limit;
        }



        return  $this->sql;
    }

    public function all($db = null)
    {




       $dbResult = $this->db->query($this->createCommand());
       unset($this->join);
       return $dbResult;

    }
   
    public function populate($rows)
    {
        if ($this->indexBy === null) {
            return $rows;
        }
        $result = [];
        foreach ($rows as $row) {
            if (is_string($this->indexBy)) {
                $key = $row[$this->indexBy];
            } else {
                $key = call_user_func($this->indexBy, $row);
            }
            $result[$key] = $row;
        }
        return $result;
    }

    public function limit($arrLimit){
        if(count($arrLimit) >1){
            $this->limit = " LIMIT ".$arrLimit[0].','.$arrLimit[1];
        } else {
            $this->limit = " LIMIT ".$arrLimit[0];
        }
        return $this;

    }




    public function one()
    {

        $this->limit([1]);
        $dbResult = $this->db->query($this->createCommand());
        return $dbResult;
    }
 
    public function scalar($db = null)
    {
        return $this->createCommand($db)->queryScalar();
    }
  
    public function column($db = null)
    {
        if (!is_string($this->indexBy)) {
            return $this->createCommand($db)->queryColumn();
        }
        if (is_array($this->select) && count($this->select) === 1) {
            $this->select[] = $this->indexBy;
        }
        $rows = $this->createCommand($db)->queryAll();
        $results = [];
        foreach ($rows as $row) {
            if (array_key_exists($this->indexBy, $row)) {
                $results[$row[$this->indexBy]] = reset($row);
            } else {
                $results[] = reset($row);
            }
        }
        return $results;
    }
  
    public function count($q = '*', $db = null)
    {
        return $this->queryScalar("COUNT($q)", $db);
    }

    public function sum($q, $db = null)
    {
        return $this->queryScalar("SUM($q)", $db);
    }
  
    public function average($q, $db = null)
    {
        return $this->queryScalar("AVG($q)", $db);
    }
  
    public function min($q, $db = null)
    {
        return $this->queryScalar("MIN($q)", $db);
    }
 
    public function max($q, $db = null)
    {
        return $this->queryScalar("MAX($q)", $db);
    }
 
    public function exists($db = null)
    {
        $command = $this->createCommand($db);
        $params = $command->params;
        $command->setSql($command->db->getQueryBuilder()->selectExists($command->getSql()));
        $command->bindValues($params);
        return (boolean)$command->queryScalar();
    }
   
    protected function queryScalar($selectExpression, $db)
    {
        $select = $this->select;
        $limit = $this->limit;
        $offset = $this->offset;
        $this->select = [$selectExpression];
        $this->limit = null;
        $this->offset = null;
        $command = $this->createCommand($db);
        $this->select = $select;
        $this->limit = $limit;
        $this->offset = $offset;
        if (empty($this->groupBy) && empty($this->having) && empty($this->union) && !$this->distinct) {
            return $command->queryScalar();
        } else {
            return (new Query)->select([$selectExpression])
                ->from(['c' => $this])
                ->createCommand($command->db)
                ->queryScalar();
        }
    }

    public function select($columns, $option = null)
    {
        if (!is_array($columns)) {
            $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
        }
        $this->select = $columns;
        $this->selectOption = $option;
        return $this;
    }
  

    public function lastId(){
        return  $this->db->getLastId();
    }


    public function insert($strTable, $arrColsToVal){
        $this->insert="INSERT INTO ".$strTable." set ";
        $strVal="";
        $z=0;
        foreach ($arrColsToVal as $col=>$value) {
            if($z > 0){
                $strVal.=',';
            }

            $strVal.= $col. "='".$this->db->escape($value) ."'";
            $z++;
        }
        $this->insert.=$strVal;

      return  $this;





    }

    public function update($strTable, $arrColsToVal){

        $this->update ="";

        $this->update="UPDATE ".$strTable." set ";
        $strVal="";
        $z=0;
        foreach ($arrColsToVal as $col=>$value) {
            if($z > 0){
                $strVal.=',';
            }

            $strVal.= $col. "='".$this->db->escape($value) ."'";
            $z++;
        }
        $this->update.=$strVal;

        return  $this;


    }



    public function addSelect($columns)
    {
        if ($columns instanceof Expression) {
            $columns = [$columns];
        } elseif (!is_array($columns)) {
            $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
        }
        if ($this->select === null) {
            $this->select = $columns;
        } else {
            $this->select = array_merge($this->select, $columns);
        }
        return $this;
    }
   
    public function distinct($value = true)
    {
        $this->distinct = $value;
        return $this;
    }
 
    public function from($tables)
    {
        if (!is_array($tables)) {
            $tables = preg_split('/\s*,\s*/', trim($tables), -1, PREG_SPLIT_NO_EMPTY);
        }
        $this->from = $tables;
        return $this;
    }

    public function where($condition, $params = [])
    {
        $this->where = $condition;
        $this->addParams($params);
        return $this;
    }

    public function andWhere($condition, $params = [])
    {
        if ($this->where === null) {
            $this->where = $condition;
        } else {
            $this->where = ['and', $this->where, $condition];
        }
        $this->addParams($params);
        return $this;
    }
  
    public function orWhere($condition, $params = [])
    {
        if ($this->where === null) {
            $this->where = $condition;
        } else {
            $this->where = ['or', $this->where, $condition];
        }
        $this->addParams($params);
        return $this;
    }
   
    public function andFilterCompare($name, $value, $defaultOperator = '=')
    {
        if (preg_match('/^(<>|>=|>|<=|<|=)/', $value, $matches)) {
            $operator = $matches[1];
            $value = substr($value, strlen($operator));
        } else {
            $operator = $defaultOperator;
        }
        return $this->andFilterWhere([$operator, $name, $value]);
    }
    
    public function join($type, $table, $on = '', $params = [])
    {
        $this->join[] = [$type, $table, $on];
        return $this->addParams($params);
    }

    public function innerJoin($table, $on = '', $params = [])
    {
        $this->join[] = [' INNER JOIN', $table, $on];
        return $this->addParams($params);
    }

    public function leftJoin($table, $on = '', $params = [])
    {
        $this->join[] = [' LEFT JOIN', $table, $on];
        return $this->addParams($params);
    }
    
    public function rightJoin($table, $on = '', $params = [])
    {
        $this->join[] = [' RIGHT JOIN', $table, $on];
        return $this->addParams($params);
    }
 
    public function groupBy($columns)
    {

        $this->groupBy = $columns;
        return $this;
    }
  
 
  
    public function having($condition, $params = [])
    {
        $this->having = $condition;
        $this->addParams($params);
        return $this;
    }
  
    public function andHaving($condition, $params = [])
    {
        if ($this->having === null) {
            $this->having = $condition;
        } else {
            $this->having = ['and', $this->having, $condition];
        }
        $this->addParams($params);
        return $this;
    }
  
    public function orHaving($condition, $params = [])
    {
        if ($this->having === null) {
            $this->having = $condition;
        } else {
            $this->having = ['or', $this->having, $condition];
        }
        $this->addParams($params);
        return $this;
    }
  
    public function union($sql, $all = false)
    {
        $this->union[] = ['query' => $sql, 'all' => $all];
        return $this;
    }
 
    public function params($params)
    {
        $this->params = $params;
        return $this;
    }
  

  
    public static function create($from)
    {
        return new self([
            'where' => $from->where,
            'limit' => $from->limit,
            'offset' => $from->offset,
            'orderBy' => $from->orderBy,
            'indexBy' => $from->indexBy,
            'select' => $from->select,
            'selectOption' => $from->selectOption,
            'distinct' => $from->distinct,
            'from' => $from->from,
            'groupBy' => $from->groupBy,
            'join' => $from->join,
            'having' => $from->having,
            'union' => $from->union,
            'params' => $from->params,
        ]);
    }


    public function order($arrOrder){
        $this->order="";
        $this->order.=" ORDER BY ".$arrOrder[0]." ".$arrOrder[1];

        return $this;



    }



}

